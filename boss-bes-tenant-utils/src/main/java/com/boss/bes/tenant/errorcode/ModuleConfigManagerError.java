package com.boss.bes.tenant.errorcode;

/**
 * 模块配置管理错误
 * @author LGX_TvT
 * @date 2019-12-15 12:25
 */
public final class ModuleConfigManagerError {

    /**
     * 增加模块配置信息不完整
     */
    public static final String MODULE_CONFIG_INSERT_DATA_INCOMPLETE_ERROR = "255001";

    /**
     * 增加模块配置信息重复
     */
    public static final String MODULE_CONFIG_INSERT_DATA_REPETITION_ERROR = "255002";

    /**
     * 修改模块配置信息不完整
     */
    public static final String MODULE_CONFIG_UPDATE_DATA_INCOMPLETE_ERROR = "255003";

    /**
     * 修改模块配置信息重复
     */
    public static final String MODULE_CONFIG_UPDATE_DATA_REPETITION_ERROR = "255004";

    /**
     * 模块配置信息不能删除
     */
    public static final String MODULE_CONFIG_DELETE_DATA_ERROR = "255005";

    /**
     * 模块信息展示错误
     */
    public static final String MODULE_CONFIG_DATA_SHOW_ERROR = "255006";

}
