package com.boss.bes.tenant.errorcode;

/**
 * 模块商城管理错误
 * @author LGX_TvT
 * @date 2019-12-15 12:26
 */
public final class ModuleStoreManagerError {

    /**
     * 模块商品信息展示错误
     */
    public static final String MODULESTORE_SHOW_ERROR = "256001";

    /**
     * 模块商品不能购买
     */
    public static final String MODULESTORE_GOODS_UNABLE_BUY_ERROR = "256002";
}
