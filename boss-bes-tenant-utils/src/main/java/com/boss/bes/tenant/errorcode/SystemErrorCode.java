package com.boss.bes.tenant.errorcode;

/**
 * 系统错误码
 * @author LGX_TvT
 * @date 2019-12-15 11:48
 */
public final class SystemErrorCode {
    // =========== 系统 ============ //
    /**
     * 未知错误
     */
    public static final String SYSTRM_UNKNOWN_ERROR = "100000";

    /**
     * 表不存在
     */
    public static final String SYSTEM_TABLE_INEXISTENCE_ERROR = "100001";

    /**
     * 请求限制
     */
    public static final String SYSTEM_REQUEST_ASTRICT_ERROR = "100002";

    /**
     * 请求超时
     */
    public static final String SYSTEM_REQUEST_TIMEOUT_ERROR = "100003";

    /**
     * 接口维护
     */
    public static final String SYSTEM_INTERFACE_PRESERVE_ERROR = "100004";

    /**
     * 接口停用
     */
    public static final String SYSTEM_INTERFACE_STOP_ERROR = "100005";

    /**
     * 系统异常
     */
    public static final String SYSTEM_ERROR = "100006";

    /**
     * 系统升级
     */
    public static final String SYSTEM_UPDATE_ERROR = "100007";
}
