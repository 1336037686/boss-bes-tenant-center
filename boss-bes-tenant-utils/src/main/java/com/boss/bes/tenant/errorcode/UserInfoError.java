package com.boss.bes.tenant.errorcode;

/**
 * 个人信息管理错误
 * @author LGX_TvT
 * @date 2019-12-15 12:23
 */
public final class UserInfoError {

    /**
     * 增加个人信息不完整
     */
    public static final String USERINFO_INSERT_DATA_INCOMPLETE_ERROR = "252001";

    /**
     * 增加个人信息重复
     */
    public static final String USERINFO_INSERT_DATA_REPETITION_ERROR = "252002";

    /**
     * 修改个人信息不完整
     */
    public static final String USERINFO_UPDATE_DATA_INCOMPLETE_ERROR = "252003";

    /**
     * 修改个人信息重复
     */
    public static final String USERINFO_UPDATE_DATA_REPETITION_ERROR = "252004";

    /**
     * 获取个人信息失败
     */
    public static final String USERINFO_DATA_SHOW_ERROR = "252005";


}
