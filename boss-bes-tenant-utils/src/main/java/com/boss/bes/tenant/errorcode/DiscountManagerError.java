package com.boss.bes.tenant.errorcode;

/**
 * 折扣管理错误
 * @author LGX_TvT
 * @date 2019-12-15 12:26
 */
public final class DiscountManagerError {

    /**
     * 增加折扣信息不完整
     */
    public static final String DISCOUNT_DATA_INCOMPLETE_ERROR = "257001";

    /**
     * 增加折扣信息重复
     */
    public static final String DISCOUNT_DATA_REPETITION_ERROR = "257002";

    /**
     * 修改折扣信息不完整
     */
    public static final String DISCOUNT_UPDATE_DATA_INCOMPLETE_ERROR = "257003";

    /**
     * 修改折扣信息重复
     */
    public static final String DISCOUNT_UPDATE_DATA_REPETITION_ERROR = "257004";

    /**
     * 折扣信息删除失败
     */
    public static final String DISCOUNT_DELETE_DATA_INCOMPLETE_ERROR = "257005";

    /**
     * 刷新折扣信息失败
     */
    public static final String DISCOUNT_DATA_FLUSH_ERROR = "257006";
}
