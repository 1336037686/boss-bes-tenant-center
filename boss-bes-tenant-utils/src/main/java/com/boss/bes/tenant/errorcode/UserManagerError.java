package com.boss.bes.tenant.errorcode;

/**
 * 用户管理错误
 * @author LGX_TvT
 * @date 2019-12-15 12:21
 */
public final class UserManagerError {

    /**
     * 用户信息不完整
     */
    public static final String USER_INSERT_DATA_INCOMPLETE_ERROR = "251001";

    /**
     * 用户信息重复
     */
    public static final String USER_INSERT_DATA_REPETITION_ERROR = "251002";

    /**
     * 修改用户信息不完整
     */
    public static final String USER_UPDATE_DATA_INCOMPLETE_ERROR = "251003";

    /**
     * 修改用户信息重复
     */
    public static final String USER_UPDATE_DATA_REPETITION_ERROR = "251004";

    /**
     * 用户信息不能删除
     */
    public static final String USER_DELETE_DATA_UNABLE_ERROR = "251005";

    /**
     * 用户信息展示错误
     */
    public static final String USER_DATA_SHOW_ERROR = "251006";
}
