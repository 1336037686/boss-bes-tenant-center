package com.boss.bes.tenant.errorcode;

/**
 * 其他界面错误
 * @author LGX_TvT
 * @date 2019-12-15 11:55
 */
public final class OtherPageError {

    /**
     * 登录信息不完整
     */
    public static final String LOGIN_DATA_INCOMPLETE_ERROR = "201001";

    /**
     * 登录信息不正确
     */
    public static final String LOGIN_DATA_INCORRECTNESS_ERROR = "201002";

    /**
     * 注册信息不完整
     */
    public static final String REGISTER_DATA_INCOMPLETE_ERROR = "202003";

    /**
     * 注册信息不正确
     */
    public static final String REGISTER_DATA_INCORRECTNESS_ERROR = "202002";

    /**
     * 改密信息不完整
     */
    public static final String PASSWORD_DATA_INCOMPLETE_ERROR = "203001";

    /**
     * 改密信息不正确
     */
    public static final String PASSWORD_DATA_INCORRECTNESS_ERROR = "203002";

}
