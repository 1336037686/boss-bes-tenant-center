package com.boss.bes.tenant.errorcode;

/**
 * 充值管理错误
 * @author LGX_TvT
 * @date 2019-12-15 12:25
 */
public final class PayManagerError {

    /**
     * 数据填写错误
     */
    public static final String PAY_DATA_INCOMPLETE_ERROR = "254001";

    /**
     * 充值失败错误
     */
    public static final String PAY_ERROR = "254002";

    /**
     * 充值信息展示失败
     */
    public static final String PAY_RECHARGE_DATA_SHOW_ERROR = "254003";

    /**
     * 余额信息展示失败
     */
    public static final String PAY_BALANCE_DATA_SHOW_ERROR = "254004";

}
