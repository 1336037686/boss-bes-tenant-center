package com.boss.bes.tenant.errorcode;

/**
 * 控制台管理错误
 * @author LGX_TvT
 * @date 2019-12-15 12:23
 */
public final class ConsoleManagerError {

    /**
     * 获取用户模块信息失败
     */
    public static final String CONSOLE_MODULE_MSG_ERROR = "253001";

}
