package com.boss.bes.tenant.entity;

import com.boss.xtrain.core.data.convention.common.BaseEntity;
import lombok.Data;

/**
 * 折扣定义
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/15 14:55
 * 4
 */
@Data
public class Discount extends BaseEntity {
    /**
     * 折扣ID
     */
    private Long id;

    /**
     * 模块ID
     */
    private Long moduleId;

    /**
     * 折扣
     */
    private Double discount;

    /**
     * 开始时间
     */
    private String startTime;

    /**
     * 结束时间
     */
    private String endTime;

    /**
     * 备注
     */
    private String remark;
}