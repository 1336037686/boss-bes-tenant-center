package com.boss.bes.tenant.mapper;

import com.boss.bes.tenant.BaseMapper;
import com.boss.bes.tenant.entity.Dictionary;

/**
 * 数据字典Mapper
 * @author LGX_TvT
 * @date 2019-12-18 19:35
 */
public interface DictionaryMapper extends BaseMapper<Dictionary> {
}
