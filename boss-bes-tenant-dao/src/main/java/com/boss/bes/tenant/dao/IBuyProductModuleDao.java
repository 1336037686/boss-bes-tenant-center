package com.boss.bes.tenant.dao;

import com.boss.bes.tenant.BaseDao;

/**
 * 模块商城dao接口
 * @author LGX_TvT
 * @date 2019-12-15 14:05
 */
public interface IBuyProductModuleDao extends BaseDao {
}
