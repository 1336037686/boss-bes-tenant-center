package com.boss.bes.tenant.entity;

import com.boss.xtrain.core.data.convention.common.BaseEntity;
import lombok.Data;

/**
 * 系统参数
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/15 14:55
 * 4
 */
@Data
public class SystemParam extends BaseEntity {
    /**
     * 资源ID
     */
    private Long id;

    /**
     * 参数类型
     */
    private String paramType;

    /**
     * 参数项
     */
    private String param;

    /**
     * 参数值
     */
    private String value;
}