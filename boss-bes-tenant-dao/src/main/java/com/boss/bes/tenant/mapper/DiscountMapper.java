package com.boss.bes.tenant.mapper;

import com.boss.bes.tenant.BaseMapper;
import com.boss.bes.tenant.entity.Discount;

/**
 * 折扣Mapper
 * @author LGX_TvT
 * @date 2019-12-18 19:35
 */
public interface DiscountMapper extends BaseMapper<Discount> {
}
