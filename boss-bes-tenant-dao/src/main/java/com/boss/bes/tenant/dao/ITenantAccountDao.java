package com.boss.bes.tenant.dao;

import com.boss.bes.tenant.BaseDao;

/**
 * 租户账号管理dao接口
 * @author LGX_TvT
 * @date 2019-12-15 14:10
 */
public interface ITenantAccountDao extends BaseDao {
}
