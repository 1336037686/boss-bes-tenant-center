package com.boss.bes.tenant.entity;

import com.boss.xtrain.core.data.convention.common.BaseEntity;
import lombok.Data;

/**
 * 购买记录明细
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/15 14:55
 * 4
 */
@Data
public class BuyRecordDetail extends BaseEntity {

    /**
     * 购买明细记录ID
     */
    private Long id;

    /**
     * 购买记录ID
     */
    private Long buyRecordId;

    /**
     *  模块ID
     */
    private Long moduleId;

    /**
     * 数量
     */
    private Double mount;

    /**
     * 折扣
     */
    private Double discount;

    /**
     * 应付金额
     */
    private Double payable;

    /**
     * 实付金额
     */
    private Double realPay;

    /**
     * 到期时间
     */
    private String invalidTime;
}