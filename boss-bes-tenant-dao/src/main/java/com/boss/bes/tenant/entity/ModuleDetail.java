package com.boss.bes.tenant.entity;

import com.boss.xtrain.core.data.convention.common.BaseEntity;
import lombok.Data;

/**
 * 模块明细
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/15 14:55
 * 4
 */
@Data
public class ModuleDetail extends BaseEntity {
    /**
     * 模块明细ID
     */
    private Long id;

    /**
     * 模块ID
     */
    private Long moduleId;

    /**
     * 资源ID
     */
    private Long resourceId;



}