package com.boss.bes.tenant;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * 基础Mapper
 * @author LGX_TvT
 * @date 2019-12-18 15:55
 */
public interface BaseMapper<T> extends Mapper<T>, MySqlMapper<T> {
}
