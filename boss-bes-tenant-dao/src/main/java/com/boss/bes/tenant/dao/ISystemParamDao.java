package com.boss.bes.tenant.dao;

import com.boss.bes.tenant.BaseDao;

/**
 * 系统参数dao接口
 * @author LGX_TvT
 * @date 2019-12-15 14:09
 */
public interface ISystemParamDao extends BaseDao {
}
