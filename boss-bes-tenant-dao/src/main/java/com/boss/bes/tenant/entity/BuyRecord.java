package com.boss.bes.tenant.entity;

import com.boss.xtrain.core.data.convention.common.BaseEntity;
import lombok.Data;

/**
 * 购买记录实体类
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/15 14:55
 * 4
 */
@Data
public class BuyRecord extends BaseEntity {

    /**
     * 购买记录_ID
     */
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 模块数量
     */
    private Integer num;

    /**
     * 应付总金额
     */
    private Double payable;

    /**
     * 总折扣
     */
    private Double discount;

    /**
     * 实付总金额
     */
    private Double realPay;

    /**
     * 支付通道
     */
    private Byte payChanel;

    /**
     * 支付时间
     */
    private String payTime;

}