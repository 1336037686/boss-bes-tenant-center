package com.boss.bes.tenant.entity;

import com.boss.xtrain.core.data.convention.common.BaseEntity;
import lombok.Data;

/**
 * @author pursuer
 */
@Data
public class Organization extends BaseEntity {
    /**
     * 组织Id
     */
    private Long id;
    /**
     * 组织名
     */
    private String name;
    /**
     * 机构编号
     */
    private String code;
    /**
     *负责人
     */
    private String master;
    /**
     * 电话
     */
    private String tel;
    /**
     * 地址
     */
    private String address;


}