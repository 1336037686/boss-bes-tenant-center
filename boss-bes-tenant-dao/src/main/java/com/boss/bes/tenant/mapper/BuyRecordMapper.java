package com.boss.bes.tenant.mapper;

import com.boss.bes.tenant.BaseMapper;
import com.boss.bes.tenant.entity.BuyRecord;

/**
 * 购买记录Mapper
 * @author LGX_TvT
 * @date 2019-12-18 19:36
 */
public interface BuyRecordMapper extends BaseMapper<BuyRecord> {
}
