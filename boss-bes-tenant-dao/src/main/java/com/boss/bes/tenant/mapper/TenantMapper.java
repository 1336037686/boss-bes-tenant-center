package com.boss.bes.tenant.mapper;

import com.boss.bes.tenant.BaseMapper;
import com.boss.bes.tenant.entity.Tenant;
import org.apache.ibatis.annotations.Mapper;

/**
 * 租户Mapper
 * @author LGX_TvT
 * @date 2019-12-18 19:33
 */
@Mapper
public interface TenantMapper extends BaseMapper<Tenant> {
}
