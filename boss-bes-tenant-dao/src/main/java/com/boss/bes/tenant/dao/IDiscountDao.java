package com.boss.bes.tenant.dao;

import com.boss.bes.tenant.BaseDao;

/**
 * 折扣管理dao接口
 * @author LGX_TvT
 * @date 2019-12-15 14:07
 */
public interface IDiscountDao extends BaseDao {
}
