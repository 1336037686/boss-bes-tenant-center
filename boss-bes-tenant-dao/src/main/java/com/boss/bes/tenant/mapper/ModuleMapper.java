package com.boss.bes.tenant.mapper;

import com.boss.bes.tenant.BaseMapper;
import com.boss.bes.tenant.entity.Module;

/**
 * 模块Mapper
 * @author LGX_TvT
 * @date 2019-12-18 19:35
 */
public interface ModuleMapper extends BaseMapper<Module> {
}
