package com.boss.bes.tenant.dao;

import com.boss.bes.tenant.BaseDao;

/**
 * 租户登陆dao接口
 * @author LGX_TvT
 * @date 2019-12-15 14:02
 */
public interface ITenantLoginDao extends BaseDao {
}
