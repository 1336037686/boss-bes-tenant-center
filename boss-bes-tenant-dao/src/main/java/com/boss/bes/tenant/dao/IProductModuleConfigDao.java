package com.boss.bes.tenant.dao;

import com.boss.bes.tenant.BaseDao;

/**
 * 模块配置dao接口
 * @author LGX_TvT
 * @date 2019-12-15 14:04
 */
public interface IProductModuleConfigDao extends BaseDao {
}
