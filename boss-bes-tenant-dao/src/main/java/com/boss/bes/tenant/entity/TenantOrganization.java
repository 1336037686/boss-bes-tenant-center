package com.boss.bes.tenant.entity;

import com.boss.xtrain.core.data.convention.common.BaseEntity;
import lombok.Data;

/**
 * @author pursuer
 */
@Data
public class TenantOrganization extends BaseEntity {
    /**
     * 主键
     */
    private Long id;
    /**
     * 机构Id
     */
    private Long organizationId;
    /**
     * 租户Id
     */
    private Long tenantId;
    /**
     * 数据库url
     */
    private String databaseAddress;

}