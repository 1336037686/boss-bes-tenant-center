package com.boss.bes.tenant.entity;

import com.boss.xtrain.core.data.convention.common.BaseEntity;
import lombok.Data;

/**
 * 模块实体
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/15 14:55
 * 4
 */
@Data
public class Module extends BaseEntity {
    /**
     * 模块ID
     */
    private Long id;

    /**
     * 模块名
     */
    private String moduleName;

    /**
     * 编号
     */
    private String code;

    /**
     * 单价
     */
    private Double price;

    /**
     * 备注
     */
    private String remark;
}