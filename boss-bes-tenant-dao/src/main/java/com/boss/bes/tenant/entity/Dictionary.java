package com.boss.bes.tenant.entity;


import com.boss.xtrain.core.data.convention.common.BaseEntity;
import lombok.Data;

/**
 * 数据字典
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/15 14:55
 * 4
 */
@Data
public class Dictionary extends BaseEntity {
    /**
     * 资源ID
     */
    private Long id;

    /**
     * 字典名
     */
    private String name;

    /**
     * 字典类型
     */
    private String category;

    /**
     * 参数值
     */
    private String value;

    /**
     * 备注
     */
    private String remark;

}