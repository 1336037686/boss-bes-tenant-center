package com.boss.bes.tenant.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 租户
 * @author pursuer
 */
@Data
public class Tenant {

    /**
     * 租户ID
     */
    private Long id;

    /**
     * 手机号
     */
    private String tel;

    /**
     * 手机号
     */
    private String password;

    /**
     * 姓名
     */
    private String tenantName;

    /**
     * 性别
     */
    private Byte sex;

    /**
     * 生日
     */
    private Date birthday;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 公司
     */
    private String company;

    /**
     * 账户余额
     */
    private BigDecimal balance;

    /**
     * 备注
     */
    private String remark;
}