package com.boss.bes.tenant.mapper;

import com.boss.bes.tenant.BaseMapper;
import com.boss.bes.tenant.entity.SystemParam;

/** 系统参数Mapper
 * @author LGX_TvT
 * @date 2019-12-18 19:34
 */
public interface SystemParamMapper extends BaseMapper<SystemParam> {
}
