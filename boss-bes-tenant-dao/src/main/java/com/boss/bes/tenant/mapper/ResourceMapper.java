package com.boss.bes.tenant.mapper;

import com.boss.bes.tenant.BaseMapper;
import com.boss.bes.tenant.entity.Resource;

/**
 * 资源Mapper
 * @author LGX_TvT
 * @date 2019-12-18 19:34
 */
public interface ResourceMapper extends BaseMapper<Resource> {
}
