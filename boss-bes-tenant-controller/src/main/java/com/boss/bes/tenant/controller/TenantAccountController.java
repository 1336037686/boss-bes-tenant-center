package com.boss.bes.tenant.controller;


import com.boss.bes.tenant.api.TenantAccountApi;

import com.boss.xtrain.core.data.convention.protocol.CommonRequest;
import com.boss.xtrain.core.data.convention.protocol.CommonResponse;
import org.springframework.web.bind.annotation.*;

/**
 * 租户账号控制类
 * @author LGX_TvT
 * @date 2019-12-15 13:59
 */
@RestController
public class TenantAccountController extends BaseController implements TenantAccountApi {
   @GetMapping("/tenant")
    @Override
    public CommonResponse doGetTenant(CommonRequest commonRequest) {
        return null;
    }
    @DeleteMapping("/tenant/{tenantId}")
    @Override
    public CommonResponse doDeleteTenantById(CommonRequest commonRequest) {
        return null;
    }
    @GetMapping("/buyRecord")
    @Override
    public CommonResponse doGetBuyRecord(CommonRequest commonRequest) {
        return null;
    }
    @GetMapping("buyRecordDetail")
    @Override
    public CommonResponse doGetBuyRecordDetail(CommonRequest commonRequest) {
        return null;
    }
    @DeleteMapping("/tenant")
    @Override
    public CommonResponse doDeleteTenant(CommonRequest commonRequest) {
        return null;
    }
    @PutMapping("/tenant")
    @Override
    public CommonResponse doUpdateTenant(CommonRequest commonRequest) {
        return null;
    }
    @PostMapping("/tenant")
    @Override
    public CommonResponse doAddTenant(CommonRequest commonRequest) {
        return null;
    }
}
