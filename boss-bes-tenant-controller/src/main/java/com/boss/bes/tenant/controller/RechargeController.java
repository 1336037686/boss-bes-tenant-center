package com.boss.bes.tenant.controller;

import com.boss.bes.tenant.api.RechargeApi;
import com.boss.bes.tenant.service.IRechargeService;
import com.boss.xtrain.core.data.convention.common.AbstractBaseController;
import com.boss.xtrain.core.data.convention.protocol.CommonRequest;
import com.boss.xtrain.core.data.convention.protocol.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/17 16:06
 * 4
 */
@Controller
public class RechargeController extends AbstractBaseController implements RechargeApi {

    @Autowired
    IRechargeService rechargeService;

    @GetMapping("/recharge")
    @Override
    public CommonResponse doGetRecharge(CommonRequest commonRequest) {
        return null;
    }
    @PostMapping("/recharge")
    @Override
    public CommonResponse doRecharge(CommonRequest commonRequest) {
        return null;
    }

    @Override
    public CommonResponse doSave(CommonRequest commonRequest) {
        return null;
    }

    @Override
    public CommonResponse doUpdate(CommonRequest commonRequest) {
        return null;
    }

    @Override
    public CommonResponse doDelete(CommonRequest commonRequest) {
        return null;
    }

    @Override
    public CommonResponse doQuery(CommonRequest commonRequest) {
        return null;
    }
}
