package com.boss.bes.tenant.controller;


import com.boss.bes.tenant.api.DiscountApi;

import com.boss.xtrain.core.data.convention.protocol.CommonRequest;
import com.boss.xtrain.core.data.convention.protocol.CommonResponse;
import org.springframework.web.bind.annotation.*;

/**
 * 折扣管理控制类
 * @author LGX_TvT
 * @date 2019-12-15 14:07
 */
@RestController
public class DiscountController extends BaseController implements DiscountApi {
    @GetMapping("/discount")
    @Override
    public CommonResponse doGetDiscount(CommonRequest commonRequest) {
        return null;
    }
    @PostMapping("/discount")
    @Override
    public CommonResponse doAddDiscount(CommonRequest commonRequest) {
        return null;
    }
    @PutMapping("/discount")
    @Override
    public CommonResponse doUpdateDiscount(CommonRequest commonRequest) {
        return null;
    }
    @DeleteMapping("/discount")
    @Override
    public CommonResponse doDeleteDiscount(CommonRequest commonRequest) {
        return null;
    }
    @DeleteMapping("/discount/{discountId}")
    @Override
    public CommonResponse doDeleteDiscountById(CommonRequest commonRequest) {
        return null;
    }
}
