package com.boss.bes.tenant.controller;

import com.boss.bes.tenant.api.SystemParamApi;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统参数控制类
 * @author LGX_TvT
 * @date 2019-12-15 14:08
 */
@RestController
public class SystemParamController extends BaseController implements SystemParamApi {
}
