package com.boss.bes.tenant.controller;

import com.boss.bes.tenant.api.BuyProductModuleApi;

import com.boss.xtrain.core.data.convention.protocol.CommonRequest;
import com.boss.xtrain.core.data.convention.protocol.CommonResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 模块商城控制类
 * @author LGX_TvT
 * @date 2019-12-15 14:05
 */
@RestController
public class BuyProductModuleController extends BaseController implements BuyProductModuleApi {
    @GetMapping("/moduleCommodity")
    @Override
    public CommonResponse doGetCommodity(CommonRequest commonRequest) {
        return null;
    }
    @PostMapping("/buyModule")
    @Override
    public CommonResponse doBuyModule(CommonRequest commonRequest) {
        return null;
    }
}
