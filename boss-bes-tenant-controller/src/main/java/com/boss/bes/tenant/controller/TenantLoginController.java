package com.boss.bes.tenant.controller;


import com.boss.bes.tenant.api.TenantLoginApi;
import com.boss.xtrain.core.data.convention.protocol.CommonRequest;
import com.boss.xtrain.core.data.convention.protocol.CommonResponse;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 租户登陆注册控制类
 * @author LGX_TvT
 * @date 2019-12-15 13:56
 */
@RestController
public class TenantLoginController extends BaseController implements TenantLoginApi {

    @Autowired
    private Admin admin;

    @GetMapping("/authTenant")
    @Override
    public CommonResponse doLogin(CommonRequest commonRequest) {
        System.out.println(admin);
        return null;
    }

    @PostMapping("/registerTenant")
    @Override
    public CommonResponse doRegister(CommonRequest commonRequest) {
        return null;
    }


}

/**
 * 由于需求暂未确定完全，老师让我们先把管理员账号写死
 * 使用配置自动注入
 */
@Data
@Component
@ConfigurationProperties("tenant.admin")
class Admin {
    private String userName;
    private String password;
}
