package com.boss.bes.tenant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@MapperScan(basePackages = {"com.boss.bes.tenant.mapper"})
@SpringBootApplication(scanBasePackages = {"com.boss.bes"})
public class BossBesTenantControllerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BossBesTenantControllerApplication.class, args);
    }

}
