package com.boss.bes.tenant.controller;


import com.boss.bes.tenant.api.ProductModuleConfigApi;

import com.boss.xtrain.core.data.convention.protocol.CommonRequest;
import com.boss.xtrain.core.data.convention.protocol.CommonResponse;
import org.springframework.web.bind.annotation.*;

/**
 * 模块配置控制类
 * @author LGX_TvT
 * @date 2019-12-15 14:02
 */
@RestController
public class ProductModuleConfigController extends BaseController implements ProductModuleConfigApi {
    @GetMapping("/module")
    @Override
    public CommonResponse doGetModule(CommonRequest commonRequest) {
        return null;
    }
    @PostMapping("/module")
    @Override
    public CommonResponse doAddModule(CommonRequest commonRequest) {
        return null;
    }
    @PutMapping("/module/{moduleId}")
    @Override
    public CommonResponse doUpdateModule(CommonRequest commonRequest) {
        return null;
    }
    @DeleteMapping("/module/moduleId")
    @Override
    public CommonResponse doDeleteModuleById(CommonRequest commonRequest) {
        return null;
    }
    @DeleteMapping("/module")
    @Override
    public CommonResponse doDeleteModule(CommonRequest commonRequest) {
        return null;
    }
}
