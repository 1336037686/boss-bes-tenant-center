package com.boss.bes.tenant.controller;

import com.boss.xtrain.common.api.logging.annotation.ApiILog;
import com.boss.xtrain.core.data.convention.common.SuccessCode;
import com.boss.xtrain.core.data.convention.protocol.CommonResponse;
import com.boss.xtrain.core.data.convention.protocol.ResponseBody;
import com.boss.xtrain.core.data.convention.protocol.ResponseHead;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 测试 前后端交互，必须开启
 * @author LGX_TvT
 * @date 2019-12-17 11:12
 */
@Api(tags = "前段测试管理")
@RestController
public class TestController {

    @ApiOperation(value="login", notes="login")
    @ApiILog
    @RequestMapping("/user/login")
    public CommonResponse login(@RequestBody UserInfo userInfo) {
        System.out.println(userInfo);
        //Msg<String>(20000, "admin-token")
        return new CommonResponse<String>(new ResponseHead("1", SuccessCode.SUCCESS_CODE, "成功", 0), new ResponseBody<String>("admin-token"));
    }

    @ApiOperation(value="getInfo", notes="getInfo")
    @ApiILog
    @RequestMapping("/user/info")
    public CommonResponse getInfo(String tocken){
        List<String> list = new ArrayList<>();
        list.add("admin");
        return new CommonResponse<Users>(new ResponseHead("1", SuccessCode.SUCCESS_CODE, "成功", 0), new ResponseBody<Users>(new Users(list, "I am a super administrator", "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif", "Super Admin")));
    }

    @ApiOperation(value="logout", notes="logout")
    @ApiILog
    @RequestMapping("/user/logout")
    public CommonResponse logout(){
        return new CommonResponse<String>(new ResponseHead("1", SuccessCode.SUCCESS_CODE, "成功", 0), new ResponseBody<String>("success"));
    }

    @ApiOperation(value="hello", notes="hello")
    @ApiILog
    @GetMapping("/hello")
    public CommonResponse hello(){
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add("List " + i);
        }
        return new CommonResponse<List<String>>(new ResponseHead("1", SuccessCode.SUCCESS_CODE, "成功", 0), new ResponseBody<List<String>>(list));
    }

}

@Data
class Msg<T> implements Serializable {
    Integer code;
    T data;

    public Msg(Integer code, T data) {
        this.code = code;
        this.data = data;
    }
}

@Data
class UserInfo {
    String username;
    String password;

    public UserInfo() {
    }

    public UserInfo(String username, String password) {
        this.username = username;
        this.password = password;
    }
}

@Data
class Users {
    List<String> roles;
    String introduction;
    String avatar;
    String name;

    public Users(List<String> roles, String introduction, String avatar, String name) {
        this.roles = roles;
        this.introduction = introduction;
        this.avatar = avatar;
        this.name = name;
    }
}