package com.boss.bes.tenant.api;

import com.boss.bes.tenant.pojo.vo.tenant.LoginVO;

import com.boss.xtrain.core.data.convention.protocol.CommonRequest;
import com.boss.xtrain.core.data.convention.protocol.CommonResponse;

/**
 * 租户登陆API
 * @author wulei
 * @date 2019-12-15 14:01
 */

public interface TenantLoginApi {
    /**
     * @param commonRequest 前端请求报文
     * @return 响应报文
     */
    CommonResponse doLogin (CommonRequest commonRequest);
    /**租户注册
     * @param commonRequest 请求报文
     * @return 响应报文
     */
    CommonResponse doRegister (CommonRequest commonRequest);
}
