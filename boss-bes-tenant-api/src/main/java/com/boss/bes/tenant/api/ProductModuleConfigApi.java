package com.boss.bes.tenant.api;


import com.boss.xtrain.core.data.convention.protocol.CommonRequest;
import com.boss.xtrain.core.data.convention.protocol.CommonResponse;

/**
 * 模块配置API
 * @author wulei
 * @date 2019-12-15 14:03
 */
public interface ProductModuleConfigApi {

    /**获取模块信息
     * @param commonRequest 请求报文
     * @return 响应报文
     */
    CommonResponse doGetModule(CommonRequest commonRequest);
    /**添加模块
     * @param commonRequest 请求报文
     * @return 响应报文
     */
    CommonResponse doAddModule(CommonRequest commonRequest);
    /**更新模块
     * @param commonRequest 请求报文
     * @return 响应报文
     */
    CommonResponse doUpdateModule(CommonRequest commonRequest);
    /**删除指定模块
     * @param commonRequest 请求报文
     * @return 响应报文
     */
    CommonResponse doDeleteModuleById(CommonRequest commonRequest);
    /**批量删除模块
     * @param commonRequest 请求报文
     * @return 响应报文
     */
    CommonResponse doDeleteModule(CommonRequest commonRequest);

}
