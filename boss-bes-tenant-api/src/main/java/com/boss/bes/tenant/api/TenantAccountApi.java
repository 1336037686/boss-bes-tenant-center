package com.boss.bes.tenant.api;


import com.boss.xtrain.core.data.convention.protocol.CommonRequest;
import com.boss.xtrain.core.data.convention.protocol.CommonResponse;

/**
 * 租户账号API
 * @author wulei
 * @date 2019-12-15 14:13
 */
public interface TenantAccountApi {
    /**获取租户信息
     * @param commonRequest 前端请求报文
     * @return
     */
    CommonResponse doGetTenant(CommonRequest commonRequest);

    /**删除指定租户
     * @param commonRequest 请求报文
     * @return 响应报文
     */
    CommonResponse doDeleteTenantById(CommonRequest commonRequest);
    /**删除指定租户
     * @param commonRequest 请求报文
     * @return 响应报文
     */
    CommonResponse doGetBuyRecord(CommonRequest commonRequest);
    /**获取租户购买记录
     * @param commonRequest 请求报文
     * @return 响应报文
     */
    CommonResponse doGetBuyRecordDetail(CommonRequest commonRequest);
    /**获取租户购买记录详情
     * @param commonRequest 请求报文
     * @return 响应报文
     */
    CommonResponse doDeleteTenant(CommonRequest commonRequest);
    /**更新指定租户
     * @param commonRequest 请求报文
     * @return 响应报文
     */
    CommonResponse doUpdateTenant(CommonRequest commonRequest);
    /**添加租户
     * @param commonRequest 请求报文
     * @return 响应报文
     */
    CommonResponse doAddTenant(CommonRequest commonRequest);



}
