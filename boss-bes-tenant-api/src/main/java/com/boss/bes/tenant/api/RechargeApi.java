package com.boss.bes.tenant.api;


import com.boss.xtrain.core.data.convention.protocol.CommonRequest;
import com.boss.xtrain.core.data.convention.protocol.CommonResponse;

/**充值api
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/17 15:51
 * 4
 */
public interface RechargeApi {

    /** 获取充值记录
     * @param commonRequest 请求报文
     * @return 响应报文
     */
    CommonResponse doGetRecharge(CommonRequest commonRequest);

    /** 充值
     * @param commonRequest 请求报文
     * @return 响应报文
     */
    CommonResponse doRecharge(CommonRequest commonRequest);
}
