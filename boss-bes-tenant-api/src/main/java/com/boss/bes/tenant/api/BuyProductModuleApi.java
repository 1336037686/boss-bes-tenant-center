package com.boss.bes.tenant.api;


import com.boss.xtrain.core.data.convention.protocol.CommonRequest;
import com.boss.xtrain.core.data.convention.protocol.CommonResponse;

/**
 * 模块商城API
 * @author wulei
 * @date 2019-12-15 14:05
 */
public interface BuyProductModuleApi {

    /**获取商品
     * @param commonRequest  请求报文
     * @return 响应报文
     */
    CommonResponse doGetCommodity(CommonRequest commonRequest);
    /**购买商品
     * @param commonRequest  请求报文
     * @return 响应报文
     */
    CommonResponse doBuyModule(CommonRequest commonRequest);


}
