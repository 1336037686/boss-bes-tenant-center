package com.boss.bes.tenant.pojo.vo.module;

import lombok.Data;

import java.util.List;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:36
 * 4
 */
@Data
public class BuyModuleVO {

    private Long tenantId;
    private Byte payChannel;
    private Double realPay;
    private Double discount;
    private Double payable;
    private Integer num;
    private List<String> moduleIds;
}
