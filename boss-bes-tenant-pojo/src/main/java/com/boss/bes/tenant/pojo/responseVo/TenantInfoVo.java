package com.boss.bes.tenant.pojo.responseVo;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/17 20:25
 * 4
 */

@Data
public class TenantInfoVo {
        private String id;
        private String tel;
        private String tenantName;
        private String sex;
        private String birthday;
        private String email;
        private String company;
        private String remark;
        private String status;
}
