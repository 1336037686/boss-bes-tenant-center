package com.boss.bes.tenant.pojo.responseVo;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/17 22:09
 * 4
 */
@Data
public class BuyRecordListVO {
    private String id;
    private String tenantId;
    private Double discount;
    private Double payable;
    private String num;
    private Double realPay;
    private Double  payChannel;
    private String patTime;
    private String status;


}
