package com.boss.bes.tenant.pojo.dto.tenant;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 9:43
 * 4
 */
@Data
public class DeleteTenantDTO {
    /**
     *
     */
    private Long tenantId;

    /**
     *
     */
    private  Long version;
}
