package com.boss.bes.tenant.pojo.responseDto;

import lombok.Data;

import java.util.List;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/17 22:04
 * 4
 */
@Data
public class DiscountListDTO {
    /**
     *
     */
    private Long id;

    /**
     *
     */
    private Long moduleId;

    /**
     *
     */
    private Double discount;

    /**
     *
     */
    private String startTime;

    /**
     *
     */
    private String endTime;

    /**
     *
     */
    private String remark;

    /**
     *
     */
    private Byte status;

    /**
     *
     */
    private List<BuyRecordDetailDTO> detail;
}
