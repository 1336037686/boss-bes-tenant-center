package com.boss.bes.tenant.pojo.dto.module;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:21
 * 4
 */
@Data
public class QueryModuleDTO {
    /**
     *
     */
    private String  moduleName;

    /**
     *
     */
    private String code;

    /**
     *
     */
    private Byte isDiscount;

    /**
     *
     */
    private Integer currentPage;

    /**
     *
     */
    private Integer size;
}
