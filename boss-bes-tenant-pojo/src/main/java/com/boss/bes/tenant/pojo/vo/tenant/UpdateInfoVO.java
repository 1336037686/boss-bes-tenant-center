package com.boss.bes.tenant.pojo.vo.tenant;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 9:55
 * 4
 */
@Data
public class UpdateInfoVO  {
    private String tel;
    private String tenantName;
    private String sex;
    private String birthday;
    private String email;
    private String company;
    private String remark;
    private String version;
}
