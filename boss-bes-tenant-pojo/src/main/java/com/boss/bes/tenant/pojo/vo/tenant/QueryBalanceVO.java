package com.boss.bes.tenant.pojo.vo.tenant;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:17
 * 4
 */
@Data
public class QueryBalanceVO {
    private Long tenantId;
}
