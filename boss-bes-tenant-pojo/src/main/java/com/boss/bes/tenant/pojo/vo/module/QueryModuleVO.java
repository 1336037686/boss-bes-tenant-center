package com.boss.bes.tenant.pojo.vo.module;

import com.boss.bes.tenant.pojo.vo.BaseVO;
import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:21
 * 4
 */
@Data
public class QueryModuleVO extends BaseVO {
    private String  moduleName;
    private String code;
    private Byte isDiscount;

}
