package com.boss.bes.tenant.pojo.vo.recharge;

import com.boss.bes.tenant.pojo.vo.BaseVO;
import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/15 14:12
 * 4
 */
@Data
public class QueryRechargeVO extends BaseVO {
    private String tenantId;
    private String startTime;
    private String endTime;
}
