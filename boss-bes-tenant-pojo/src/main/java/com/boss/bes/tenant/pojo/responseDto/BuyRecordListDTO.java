package com.boss.bes.tenant.pojo.responseDto;

import lombok.Data;

import java.util.List;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/17 22:09
 * 4
 */
@Data
public class BuyRecordListDTO {
    /**
     *
     */
    private Long id;

    /**
     *
     */
    private Long tenantId;

    /**
     *
     */
    private Double discount;

    /**
     *
     */
    private Double payable;

    /**
     *
     */
    private Integer num;

    /**
     *
     */
    private Double realPay;

    /**
     *
     */
    private Double  payChannel;

    /**
     *
     */
    private String patTime;

    /**
     *
     */
    private Byte status;

    /**
     *
     */
    private List<BuyRecordListDTO> detail;

}
