package com.boss.bes.tenant.pojo.dto.discount;


import com.boss.xtrain.core.data.convention.common.BaseDTO;
import lombok.Data;

import java.util.List;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:43
 * 4
 */
@Data
public class SaveDiscountDTO extends BaseDTO {
    /**
     *
     */
    private Long discountId;

    /**
     *
     */
    private List<Long> moduleIds;

    /**
     *
     */
    private Double Discount;

    /**
     *
     */
    private String startTime;

    /**
     *
     */
    private String endTime;

    /**
     *
     */
    private Integer status;

    /**
     *
     */
    private String remark;

}
