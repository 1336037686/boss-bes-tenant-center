package com.boss.bes.tenant.pojo.dto.tenant;


import com.boss.xtrain.core.data.convention.common.BaseDTO;
import lombok.Data;

/**
 * @author wulei
 */
@Data
public class RegisterDTO extends BaseDTO {
    private  Long tenantId;
    private String tel;
    private String captcha;
    private String password;
    private String tenantName;
    private String company;
}
