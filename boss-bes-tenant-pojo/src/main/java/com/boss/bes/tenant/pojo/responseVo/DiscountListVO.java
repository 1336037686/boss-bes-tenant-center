package com.boss.bes.tenant.pojo.responseVo;

import lombok.Data;

import java.util.List;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/17 22:04
 * 4
 */
@Data
public class DiscountListVO {
    private String id;
    private String moduleId;
    private Double discount;
    private String startTime;
    private String endTime;
    private String remark;
    private String status;
}
