package com.boss.bes.tenant.pojo.dto.discount;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:42
 * 4
 */
@Data
public class DeleteDiscountDTO {
    /**
     *
     */
    private Long discountId;

    /**
     *
     */
    private Long version;
}
