package com.boss.bes.tenant.pojo.dto.tenant;

import lombok.Data;

/**
 * @author wulei
 */
@Data
public class TenantDTO {

    private String tel;
    private String tenantName;
    private String company;
    private String remark;
    private Long version;
}
