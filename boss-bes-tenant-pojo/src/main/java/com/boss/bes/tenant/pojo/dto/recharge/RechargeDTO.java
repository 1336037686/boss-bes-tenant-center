package com.boss.bes.tenant.pojo.dto.recharge;


import com.boss.xtrain.core.data.convention.common.BaseDTO;
import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:14
 * 4
 */
@Data
public class RechargeDTO extends BaseDTO {
    /**
     *
     */
    private Double amount;

    /**
     *
     */
    private Long tenantId;

    /**
     *
     */
    private String channel;
}
