package com.boss.bes.tenant.pojo.vo.tenant;


import com.boss.bes.tenant.pojo.vo.BaseVO;
import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 9:36
 * 4
 */
@Data
public class QueryTenantVO extends BaseVO {
    private String tenantId;
    private String tel;
    private String tenantName;
    private  String company;

}
