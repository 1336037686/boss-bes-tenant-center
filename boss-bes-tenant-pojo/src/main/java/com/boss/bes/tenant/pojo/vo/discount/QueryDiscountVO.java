package com.boss.bes.tenant.pojo.vo.discount;

import com.boss.bes.tenant.pojo.vo.BaseVO;
import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:40
 * 4
 */
@Data
public class QueryDiscountVO extends BaseVO {
    private String moduleName;
    private String moduleCode;
    private String startTime;
    private String endTime;
    private Integer status;

}
