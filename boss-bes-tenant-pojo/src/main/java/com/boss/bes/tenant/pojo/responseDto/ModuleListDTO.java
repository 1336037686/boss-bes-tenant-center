package com.boss.bes.tenant.pojo.responseDto;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/17 22:01
 * 4
 */
@Data
public class ModuleListDTO {
    /**
     *
     */
    private Long id;

    /**
     *
     */
    private String moduleName;

    /**
     *
     */
    private String code;

    /**
     *
     */
    private Double price;

    /**
     *
     */
    private Double discount;

    /**
     *
     */
    private String startTime;

    /**
     *
     */
    private String endTime;

    /**
     *
     */
    private String remark;

    /**
     *
     */
    private Byte status;
}
