package com.boss.bes.tenant.pojo.dto.discount;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:40
 * 4
 */
public class QueryDiscountDTO {
    /**
     *
     */
    private String moduleName;

    /**
     *
     */
    private String moduleCode;

    /**
     *
     */
    private String startTime;

    /**
     *
     */
    private String endTime;

    /**
     *
     */
    private Integer status;

    /**
     *
     */
    private Integer currentPage;

    /**
     *
     */
    private Integer size;
}
