package com.boss.bes.tenant.pojo.vo.tenant;

import lombok.Data;

/**
 * @author wulei
 */
@Data
public class LoginVO {
    private String tel;
    private String password;
}
