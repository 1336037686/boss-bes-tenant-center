package com.boss.bes.tenant.pojo.vo.discount;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/15 14:14
 * 4
 */
@Data
public class UpdateDiscountVO {
    private String discountId;
    private Double discount;
    private String startTime;
    private String endTime;
    private Byte status;
    private String version;

}
