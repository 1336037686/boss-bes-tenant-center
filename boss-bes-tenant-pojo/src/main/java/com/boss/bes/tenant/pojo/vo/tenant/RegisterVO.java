package com.boss.bes.tenant.pojo.vo.tenant;


import lombok.Data;

/**
 * @author wulei
 */
@Data
public class RegisterVO   {
    private String tel;
    private String captcha;
    private String password;
    private String tenantName;
    private String company;
}
