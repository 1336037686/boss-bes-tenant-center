package com.boss.bes.tenant.pojo.vo.discount;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:42
 * 4
 */
@Data
public class DeleteDiscountVO {
    private Long discountId;
    private Long version;
}
