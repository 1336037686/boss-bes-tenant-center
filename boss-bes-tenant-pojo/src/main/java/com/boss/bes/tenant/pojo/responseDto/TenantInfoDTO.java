package com.boss.bes.tenant.pojo.responseDto;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/17 20:25
 * 4
 */

@Data
public class TenantInfoDTO {
    /**
     *
     */
    private Long id;

    /**
     *
     */
    private String tel;

    /**
     *
     */
    private String tenantName;

    /**
     *
     */
    private Byte sex;

    /**
     *
     */
    private String birthday;

    /**
     *
     */
    private String email;

    /**
     *
     */
    private String company;

    /**
     *
     */
    private String remark;

    /**
     *
     */
    private Byte status;
}
