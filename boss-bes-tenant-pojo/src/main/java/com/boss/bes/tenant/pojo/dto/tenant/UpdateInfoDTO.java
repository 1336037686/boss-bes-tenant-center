package com.boss.bes.tenant.pojo.dto.tenant;

import com.boss.xtrain.core.data.convention.common.BaseDTO;
import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 9:55
 * 4
 */
@Data
public class UpdateInfoDTO extends BaseDTO {
    private String tel;
    private String tenantName;
    private String sex;
    private String birthday;
    private String email;
    private String company;
    private String remark;

}
