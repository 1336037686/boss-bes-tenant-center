package com.boss.bes.tenant.pojo.vo.module;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:29
 * 4
 */
@Data
public class DeleteModuleVO {
    private Long moduleId;
    private Long version;
}
