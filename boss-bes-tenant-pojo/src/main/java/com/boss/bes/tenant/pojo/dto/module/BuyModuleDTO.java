package com.boss.bes.tenant.pojo.dto.module;


import com.boss.xtrain.core.data.convention.common.BaseDTO;
import lombok.Data;

import java.util.List;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:36
 * 4
 */
@Data
public class BuyModuleDTO extends BaseDTO {
    /**
     *
     */
    private Long buyRecordId;

    /**
     *
     */
    private Long tenantId;

    /**
     *
     */
    private Byte payChannel;

    /**
     *
     */
    private Double realPay;

    /**
     *
     */
    private Double discount;

    /**
     *
     */
    private Double payable;

    /**
     *
     */
    private Integer num;

    /**
     *
     */
    private List<Long> moduleIds;

    /**
     *
     */
    private List<Long>buyRecordDetailIds;
}
