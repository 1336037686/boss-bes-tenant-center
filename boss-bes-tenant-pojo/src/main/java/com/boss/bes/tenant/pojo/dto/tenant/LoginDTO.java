package com.boss.bes.tenant.pojo.dto.tenant;

import lombok.Data;

/**
 * @author wulei
 */
@Data
public class LoginDTO {
    /**
     *
     */
    private String tel;

    /**
     *
     */
    private String password;
}
