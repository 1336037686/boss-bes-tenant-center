package com.boss.bes.tenant.pojo.vo.tenant;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 9:43
 * 4
 */
@Data
public class DeleteTenantVO {
    private Long tenantId;
    private  Long version;
}
