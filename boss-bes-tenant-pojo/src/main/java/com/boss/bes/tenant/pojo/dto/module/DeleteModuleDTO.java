package com.boss.bes.tenant.pojo.dto.module;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:29
 * 4
 */
@Data
public class DeleteModuleDTO {
    /**
     *
     */
    private Long moduleId;

    /**
     *
     */
    private Long version;
}
