package com.boss.bes.tenant.pojo.dto.tenant;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 9:36
 * 4
 */
@Data
public class QueryTenantDTO {
    /**
     *
     */
    private String tenantId;

    /**
     *
     */
    private String tel;

    /**
     *
     */
    private String tenantName;

    /**
     *
     */
    private  String company;

    /**
     *
     */
    private Integer currentPage;

    /**
     *
     */
    private Integer size;
}
