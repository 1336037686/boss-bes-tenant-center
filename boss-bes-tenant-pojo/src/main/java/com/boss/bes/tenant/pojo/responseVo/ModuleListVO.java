package com.boss.bes.tenant.pojo.responseVo;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/17 22:01
 * 4
 */
@Data
public class ModuleListVO {
    private String id;
    private String moduleName;
    private String code;
    private Double price;
    private Double discount;
    private String startTime;
    private String endTime;
    private String remark;
    private String status;
}
