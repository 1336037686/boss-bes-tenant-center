package com.boss.bes.tenant.pojo.dto.discount;

import com.boss.xtrain.core.data.convention.common.BaseDTO;
import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/15 14:23
 * 4
 */
@Data
public class UpdateDiscountDTO extends BaseDTO {
    /**
     *
     */
    private String discountId;

    /**
     *
     */
    private Double discount;

    /**
     *
     */
    private String startTime;

    /**
     *
     */
    private String endTime;

    /**
     *
     */
    private Byte status;
}
