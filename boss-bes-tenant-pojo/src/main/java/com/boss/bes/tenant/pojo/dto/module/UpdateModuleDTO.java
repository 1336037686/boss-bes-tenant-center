package com.boss.bes.tenant.pojo.dto.module;


import com.boss.xtrain.core.data.convention.common.BaseDTO;
import lombok.Data;

import java.util.List;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:24
 * 4
 */
@Data
public class UpdateModuleDTO extends BaseDTO {
    /**
     *
     */
    private Long moduleId;

    /**
     *
     */
    private String moduleName;

    /**
     *
     */
    private String code;

    /**
     *
     */
    private String price;

    /**
     *
     */
    private String discount;

    /**
     *
     */
    private  String startTime;

    /**
     *
     */
    private String endTime;

    /**
     *
     */
    private String remark;

    /**
     *
     */
    private String status;

    /**
     *
     */
    private List<Long> resourceIds;
}
