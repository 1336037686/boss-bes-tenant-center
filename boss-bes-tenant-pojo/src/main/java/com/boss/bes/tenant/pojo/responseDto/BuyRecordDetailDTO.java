package com.boss.bes.tenant.pojo.responseDto;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/17 22:33
 * 4
 */
@Data
public class BuyRecordDetailDTO {
    /**
     *
     */
    private  Long moduleId;

    /**
     *
     */
    private  Long moduleName;

    /**
     *
     */
    private  Integer mount;

    /**
     *
     */
    private  Double discount;

    /**
     *
     */
    private  Double payable;

    /**
     *
     */
    private  Double realPay;

    /**
     *
     */
    private  String invalidTime;

    /**
     *
     */
    private  Byte status;
}
