package com.boss.bes.tenant.pojo.vo.module;

import com.boss.bes.tenant.pojo.vo.BaseVO;
import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:39
 * 4
 */
@Data
public class QueryOrderVO extends BaseVO {
     private  Long tenantId;
}

