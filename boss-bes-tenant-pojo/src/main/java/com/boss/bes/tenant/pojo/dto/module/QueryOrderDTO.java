package com.boss.bes.tenant.pojo.dto.module;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:39
 * 4
 */
@Data
public class QueryOrderDTO {
     /**
      *
      */
     private  Long tenantId;

     /**
      *
      */
     private Integer currentPage;

     /**
      *
      */
     private Integer size;
}

