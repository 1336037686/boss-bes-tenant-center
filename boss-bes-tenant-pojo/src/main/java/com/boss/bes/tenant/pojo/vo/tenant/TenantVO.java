package com.boss.bes.tenant.pojo.vo.tenant;

import lombok.Data;

/**
 * @author wulei
 */
@Data
public class TenantVO {
private String tel;
private String tenantName;
private String company;
private String remark;
private Long version;
}
