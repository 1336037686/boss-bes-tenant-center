package com.boss.bes.tenant.pojo.responseVo;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:14
 * 4
 */
@Data
public class RechargeListVO {
    private String tenantId;
    private String tenantName;
    private String createdTime;
    private String channel;
    private Double amount;
}
