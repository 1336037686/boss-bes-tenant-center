package com.boss.bes.tenant.pojo.vo;

import lombok.Data;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 9:49
 * 4
 */
@Data
public class BaseVO {
    private Integer currentPage;
    private  Integer size;
}
