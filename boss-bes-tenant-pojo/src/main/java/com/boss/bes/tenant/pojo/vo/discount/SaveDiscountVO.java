package com.boss.bes.tenant.pojo.vo.discount;

import lombok.Data;

import java.util.List;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 10:43
 * 4
 */
@Data
public class SaveDiscountVO   {
    private List<Long> moduleIds;
    private Double Discount;
    private String startTime;
    private String endTime;
    private Integer status;
    private String remark;

}
